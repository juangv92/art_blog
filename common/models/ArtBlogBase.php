<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;


/**
 * Base model
 */
class ArtBlogBase extends ActiveRecord
{
    const STATUS_ACTIVE = true;
	const STATUS_INACTIVE = false;

}
