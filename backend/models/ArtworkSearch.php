<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Artwork;

/**
 * ArtworkSearch represents the model behind the search form of `backend\models\Artwork`.
 */
class ArtworkSearch extends Artwork
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'artwork_type_id', 'exhibition_id', 'technique_id', 'm_unit_id', 'quantity'], 'integer'],
            [['name', 'file_name', 'abstract', 'description', 'release_date', 'availability', 'status', 'created_at', 'updated_at'], 'safe'],
            [['price', 'width', 'height'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Artwork::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'author_id' => $this->author_id,
            'artwork_type_id' => $this->artwork_type_id,
            'exhibition_id' => $this->exhibition_id,
            'technique_id' => $this->technique_id,
            'm_unit_id' => $this->m_unit_id,
            'DATE(release_date)' => $this->release_date,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'width' => $this->width,
            'height' => $this->height,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'abstract', $this->abstract])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'availability', $this->availability])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
