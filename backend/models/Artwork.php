<?php

namespace backend\models;

use common\models\ArtBlogBase;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%artwork}}".
 *
 * @property int $id
 * @property string $name
 * @property int $author_id
 * @property int $artwork_type_id
 * @property int $exhibition_id
 * @property int $technique_id
 * @property int $m_unit_id
 * @property string $file_name
 * @property string $abstract
 * @property string $description
 * @property string $release_date
 * @property int $availability
 * @property int $quantity
 * @property double $price
 * @property double $raiting
 * @property double $width
 * @property double $height
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 *
 * @property MeasurementUnit $mUnit
 * @property ArtworkType $artworkType
 * @property Exhibition $exhibition
 * @property Technique $technique
 * @property Curriculum $author
 * @property Review[] $reviews
 */
class Artwork extends ArtBlogBase
{
    public $file;

    const AVAILABILITY_STATUS = true;
    const UNAVAILABILITY_STATUS = false;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%artwork}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'artwork_type_id', 'technique_id', 'm_unit_id', 'abstract'], 'required'],
            [['author_id', 'artwork_type_id', 'exhibition_id', 'technique_id', 'm_unit_id', 'quantity'], 'integer'],
            [['description'], 'string'],
            [['file'], 'file'],
            [['release_date', 'created_at', 'updated_at'], 'safe'],
            [['price', 'raiting', 'width', 'height'], 'number'],
            [['name', 'file_name', 'abstract'], 'string', 'max' => 255],
            [['availability', 'status'], 'string', 'max' => 1],
            [['m_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeasurementUnit::className(), 'targetAttribute' => ['m_unit_id' => 'id']],
            [['artwork_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArtworkType::className(), 'targetAttribute' => ['artwork_type_id' => 'id']],
            [['exhibition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Exhibition::className(), 'targetAttribute' => ['exhibition_id' => 'id']],
            [['technique_id'], 'exist', 'skipOnError' => true, 'targetClass' => Technique::className(), 'targetAttribute' => ['technique_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Curriculum::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nombre',
            'author_id' => 'Autor',
            'artwork_type_id' => 'Tipo de Obra',
            'exhibition_id' => 'Exhibición',
            'technique_id' => 'Técnica',
            'm_unit_id' => 'UM',
            'file_name' => 'Poster',
            'file' => 'Poster',
            'abstract' => 'Resumen',
            'description' => 'Descripción',
            'release_date' => 'Fecha de Exposición',
            'availability' => 'Disponibilidad',
            'quantity' => 'Cantidad',
            'price' => 'Precio',
            'raiting' => 'Raiting',
            'width' => 'Ancho',
            'height' => 'Alto',
            'created_at' => 'Fecha de Creado',
            'updated_at' => 'Fecha de Actualizado',
            'status' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMUnit()
    {
        return $this->hasOne(MeasurementUnit::className(), ['id' => 'm_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtworkType()
    {
        return $this->hasOne(ArtworkType::className(), ['id' => 'artwork_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExhibition()
    {
        return $this->hasOne(Exhibition::className(), ['id' => 'exhibition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnique()
    {
        return $this->hasOne(Technique::className(), ['id' => 'technique_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Curriculum::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['artwork_id' => 'id']);
    }

    /**
     * Manage Created and Update times, initialize file object to be processed by controller
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        //Manage Created and updated times
        if ($this->isNewRecord) {
            $this->created_at = date("Y-m-d H:i:s");
            $this->updated_at = date("Y-m-d H:i:s");
            $this->author_id = Curriculum::getActiveCurriculumID();
        } else {
            $this->updated_at = date("Y-m-d H:i:s");
        }

        $this->file = UploadedFile::getInstance($this, 'file');

        if ($this->isNewRecord && !isset($this->file)) {
            $this->addError('file', "Este campo no puede estar vacío.");
            return false;
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /** Return true if the current model is Active, false other wise
     * @return bool
     */
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /** Return true if poster (file_name) has some value, false other wise
     * @return bool
     */
    public function hasPoster()
    {
        return isset($this->file_name) && !empty($this->file_name);
    }

    /** Return if the current model is Available or Not
     * @return bool return true if is Active, false other wise
     */
    public function isAvailable()
    {
        return $this->availability == self::AVAILABILITY_STATUS;
    }

    /** Return a mapped array of Artworks in [id=>name] format
     * @return array
     */
    public static function getMappedArtworks()
    {
        return ArrayHelper::map(self::find()
            ->where(['status' => self::STATUS_ACTIVE])
            ->all(), 'id', 'name');
    }

    /** Returns the relative path to avatar file
     * @return string
     */
    public function generateFileRoute()
    {
        return Yii::$app->getHomeUrl() .
        '../../common/resources/artwork_files/Artwork_' . $this->id . "/" . $this->file_name;
    }

    public function beforeDelete()
    {
        //We need to remove files generated trough model live cycle 
        $directory_resources = '../../common/resources/artwork_files/Artwork_' . $this->id;
        FileHelper::removeDirectory($directory_resources);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function getDimension(){
        return $this->width . 'x' . $this->height . ' ' . $this->mUnit->name ;
    }
}
