<?php

namespace backend\controllers;

use backend\models\Curriculum;
use Yii;
use backend\models\Contact;
use backend\models\ContactSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Contact model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contact model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contact();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        if (!isset($model->status)) $model->status = Contact::STATUS_ACTIVE;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Contact model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contact model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contact the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contact::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Change the status of Contact model with the param 'id'
     * @param $id integer The Contact ID to change it status
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionStatus($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (($model = $this->findModel($id)) !== null) {
                Yii::$app->db->createCommand()
                    ->update(Contact::tableName(), [
                        'status' => $model->isActive() ? Contact::STATUS_INACTIVE : Contact::STATUS_ACTIVE
                    ], ['id' => $id])->execute();

            }
            return "Status has Changed";
        }

        return "ERROR: some error has intercept the operation.";
    }

    /**
     * Change the primary status of Contact model with the param 'id'
     * @param $id integer The Contact ID to change it status
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionPrimary($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (($model = $this->findModel($id)) !== null) {
                // There is only ONE primary contact, so we handle that condition
                if($model->isPrimary()){
                    //Just change it primary status to secondary
                    Yii::$app->db->createCommand()
                        ->update('contact', ['primary'=>Contact::SECONDARY_STATUS],['id'=>$id])->execute();
                }else{
                    //We need to update the current primary model to disable it
                    $primary = Contact::getPrimaryContactForCurriculum(Curriculum::getActiveCurriculumID());
                    if(isset($primary)){
                        Yii::$app->db->createCommand()
                            ->update('contact', ['primary'=>Contact::SECONDARY_STATUS],['id'=>$primary->id])->execute();
                    }
                    //Only then we can change model to primary status
                    Yii::$app->db->createCommand()
                        ->update('contact', ['primary'=>Contact::PRIMARY_STATUS],['id'=>$id])->execute();
                }
            }
            return "Status has Changed";
        }

        return "ERROR: some error has intercept the operation.";
    }
}
