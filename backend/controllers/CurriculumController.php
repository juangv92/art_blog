<?php

namespace backend\controllers;

use Yii;
use backend\models\Curriculum;
use backend\models\CurriculumSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CurriculumController implements the CRUD actions for Curriculum model.
 */
class CurriculumController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Curriculum models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CurriculumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Curriculum model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Curriculum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Curriculum();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!$this->manageFiles($model)) {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            return $this->redirect(['index']);
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Curriculum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!$this->manageFiles($model)) {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Curriculum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Curriculum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Curriculum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Curriculum::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Change the status of Curriculum model with the param 'id'
     * @param $id integer The Curriculum ID to change it status
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionStatus($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (($model = $this->findModel($id)) !== null) {
                // There is only ONE active Curriculum, so we handle that condition
                if ($model->isActive()) {
                    //Just change it status
                    Yii::$app->db->createCommand()
                        ->update(Curriculum::tableName(), ['status' => Curriculum::STATUS_INACTIVE], ['id' => $id])->execute();
                } else {
                    //We need to update the current curriculum activated to disable it
                    $currID = Curriculum::getActiveCurriculumID();
                    if ($currID > 0) {
                        Yii::$app->db->createCommand()
                            ->update(Curriculum::tableName(), ['status' => Curriculum::STATUS_INACTIVE], ['id' => $currID])->execute();
                    }
                    //Only then we can change model to status Active
                    Yii::$app->db->createCommand()
                        ->update(Curriculum::tableName(), ['status' => Curriculum::STATUS_ACTIVE], ['id' => $id])->execute();
                }
            }
            return "Status has Changed";
        }

        return "ERROR: some error has intercept the operation.";
    }

    /** Manage the save or replace avatar file related with a Curriculum model
     * @param Curriculum $model
     * @return bool|int
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    protected function manageFiles(Curriculum $model)
    {
        $route = '../../common/resources/curriculum_files/Curriculum_' . $model->id . "/";
        if (!file_exists($route)) {
            FileHelper::createDirectory($route, 0777);
        }

        if (isset($model->file)) {
            if (isset($model->avatar) && file_exists($route . $model->avatar)) {
                unlink($route . $model->avatar);
            }
            $model->avatar = "Avatar_" . time() . "." . $model->file->getExtension();
            $model->file->saveAs($route . $model->avatar);
            return Yii::$app->db->createCommand()
                ->update(Curriculum::tableName(), ['avatar' => $model->avatar], ['id' => $model->id])->execute();
        }

        return true;
    }
}
