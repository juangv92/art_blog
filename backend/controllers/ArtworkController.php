<?php

namespace backend\controllers;

use Yii;
use backend\models\Artwork;
use backend\models\ArtworkSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ArtworkController implements the CRUD actions for Artwork model.
 */
class ArtworkController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Artwork models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArtworkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Artwork model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Artwork model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Artwork();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!$this->manageFiles($model)) {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            return $this->redirect(['index']);
        }

        if (!isset($model->status)) $model->status = Artwork::STATUS_ACTIVE;
        if (!isset($model->availability)) $model->availability = Artwork::AVAILABILITY_STATUS;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Artwork model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!$this->manageFiles($model)) {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Artwork model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Artwork model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Artwork the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Artwork::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Change the status of Artwork model with the param 'id'
     * @param $id integer The Artwork ID to change it status
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionStatus($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (($model = $this->findModel($id)) !== null) {
                Yii::$app->db->createCommand()
                    ->update('artwork', [
                        'status' => $model->isActive() ? Artwork::STATUS_INACTIVE : Artwork::STATUS_ACTIVE
                    ], ['id' => $id])->execute();

            }
            return "Status has Changed";
        }

        return "ERROR: some error has intercept the operation.";
    }

    /**
     * Change the status of Artwork model with the param 'id'
     * @param $id integer The Artwork ID to change it status
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionAvailability($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (($model = $this->findModel($id)) !== null) {
                Yii::$app->db->createCommand()
                    ->update(Artwork::tableName(), [
                        'availability' => $model->isAvailable() ? Artwork::UNAVAILABILITY_STATUS: Artwork::AVAILABILITY_STATUS
                    ], ['id' => $id])->execute();

            }
            return "Availability has Changed";
        }

        return "ERROR: some error has intercept the operation.";
    }

    /** Manage the save or replace file related with an Artwork model
     * @param Artwork $model
     * @return bool|int
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    protected function manageFiles(Artwork $model)
    {
        $route =  '../../common/resources/artwork_files/Artwork_' . $model->id . "/";
        if (!file_exists($route)) {
            FileHelper::createDirectory($route, 0777);
        }

        if (isset($model->file)) {
            if (isset($model->file_name) && file_exists($route . $model->file_name)) {
                unlink($route . $model->file_name);
            }
            $model->file_name = "ArtworkFile_" . time() . "." . $model->file->getExtension();
            $model->file->saveAs($route . $model->file_name);
            return Yii::$app->db->createCommand()
                ->update(Artwork::tableName(), ['file_name' => $model->file_name], ['id' => $model->id])->execute();
        }

        return true;
    }
}
