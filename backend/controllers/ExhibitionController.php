<?php

namespace backend\controllers;

use Yii;
use backend\models\Exhibition;
use backend\models\ExhibitionSearch;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ExhibitionController implements the CRUD actions for Exhibition model.
 */
class ExhibitionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Exhibition models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExhibitionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Exhibition model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Exhibition model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Exhibition();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!$this->manageFiles($model)) {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            return $this->redirect(['index']);
        }

        if(!isset($model->status)) $model->status = Exhibition::STATUS_ACTIVE;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Exhibition model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!$this->manageFiles($model)) {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Exhibition model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Exhibition model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Exhibition the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Exhibition::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Change the status of Exhibition model with the param 'id'
     * @param $id integer The Artwork ID to change it status
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionStatus($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (($model = $this->findModel($id)) !== null) {
                Yii::$app->db->createCommand()
                    ->update(Exhibition::tableName(), [
                        'status' => $model->isActive() ? Exhibition::STATUS_INACTIVE : Exhibition::STATUS_ACTIVE
                    ], ['id' => $id])->execute();

            }
            return "Status has Changed";
        }

        return "ERROR: some error has intercept the operation.";
    }

    /** Manage the save or replace poster file related with an Exhibition model
     * @param Exhibition $model
     * @return bool|int
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    protected function manageFiles(Exhibition $model)
    {
        $route = '../../common/resources/exhibition_files/Event_' . $model->id . "/";
        if (!file_exists($route)) {
            FileHelper::createDirectory($route, 0777);
        }

        if (isset($model->file)) {
            if (isset($model->poster) && file_exists($route . $model->poster)) {
                unlink($route . $model->poster);
            }
            $model->poster = "EventFile_" . time() . "." . $model->file->getExtension();
            $model->file->saveAs($route . $model->poster);
            return Yii::$app->db->createCommand()
                ->update(Exhibition::tableName(), ['poster'=>$model->poster], ['id' => $model->id])->execute();
        }

        return true;
    }
}
