<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Technique */

$this->title = 'Crear Técnica';
$this->params['breadcrumbs'][] = ['label' => 'Técnicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="technique-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
