<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Technique */

$this->title = 'Actualizar Técnica: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Técnicas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="technique-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
