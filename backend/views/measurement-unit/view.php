<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\MeasurementUnit */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Unidades de Medida', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="measurement-unit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Crear', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Seguro desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'created_at',
            'updated_at',
            [
                'attribute' => 'status',
                'value' => function (\backend\models\MeasurementUnit $model) {
                    $url = \yii\helpers\Url::to(['/measurement-unit/status', 'id' => $model->id]);
                    return \kartik\switchinput\SwitchInput::widget([
                        'name' => 'status',
                        'value' => $model->status,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => 'Activo',
                            'offText' => 'Inactivo',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() {
                                        $.ajax('$url', {
                                            type: 'POST'
                                        }).done(function(data) {
                                            console.log(data);
                                        });
                                    }",
                        ]
                    ]);
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
