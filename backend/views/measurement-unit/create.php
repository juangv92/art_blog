<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\MeasurementUnit */

$this->title = 'Crear Unidad de Medida';
$this->params['breadcrumbs'][] = ['label' => 'Unidades de Medida', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="measurement-unit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
