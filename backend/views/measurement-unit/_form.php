<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use webvimark\modules\UserManagement\models\User;
use common\models\ArtBlogBase;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model backend\models\MeasurementUnit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="measurement-unit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->widget(SwitchInput::classname(),[
            'name' => 'status',
            'pluginOptions' => ['size' => 'medium',
                'onText'=>'Activo',
                'offText'=>'Inactivo',
            ],
        ]);?>
    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
        <?= Html::a('Cancelar', Yii::$app->request->getReferrer(), ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
