<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MeasurementUnit */

$this->title = 'Actualizar Unidad de Medida: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Unidades de Medida', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="measurement-unit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
