<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ExhibitionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Exhibiciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exhibition-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Exhibición', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id'=>'eventsGrid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'table-responsive'
        ],
        'columns' => [
            [
                'attribute' => 'id',
                'value' => 'id',
                'contentOptions' => ['style' => 'width:5%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'name',
                'value' => 'name',
                'contentOptions' => ['style' => 'width:10%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'event_date',
                'value' => 'event_date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'event_date',
                    'language' => 'es',
                ]),
                'format' => 'html',
                'contentOptions' => ['style' => 'width:10%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'event_time',
                'value' => 'event_time',
                /*'filter' => TimePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'event_time',
                ]),*/
                'format' => 'html',
                'contentOptions' => ['style' => 'width:10%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'event_time_average',
                'value' => 'event_time_average',
                'contentOptions' => ['style' => 'width:5%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'place',
                'value' => 'place',
                'contentOptions' => ['style' => 'width:8%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'status',
                'filter' => ["1" => "Activo", "0" => "Inactivo"],
                'value' => function (\backend\models\Exhibition $model) {
                    $url = \yii\helpers\Url::to(['/exhibition/status', 'id' => $model->id]);
                    return \kartik\widgets\SwitchInput::widget([
                        'name' => 'status',
                        'value' => $model->status,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() { 
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        console.log(data);
                                        $.pjax.reload({container: '#eventsGrid'});
                                    });
                                }",
                        ]
                    ]);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:8%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'contentOptions' => ['style' => 'width:6%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
