<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use webvimark\modules\UserManagement\models\User;
use common\models\ArtBlogBase;
use kartik\widgets\SwitchInput;
use kartik\time\TimePicker;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Exhibition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exhibition-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'abstract')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(\yii\redactor\widgets\Redactor::className(),[
        'clientOptions' => [
            'lang' => 'es',
            'plugins' => ['clips', 'fontcolor', 'imagemanager'],
            'placeholder'=>'Descripción del Evento'
        ]
    ]); ?>

    <?= $form->field($model, 'file')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            'showRemove' => false,
            'showPreview' => !$model->isNewRecord && $model->hasPoster(),
            'initialPreview' => [$model->generateFileRoute()],
            'initialPreviewConfig' => [
                ['caption' => $model->poster],
            ],
            'initialPreviewAsData' => true,
            'overwriteInitial' => !$model->isNewRecord && $model->hasPoster(),
            'initialCaption' => $model->isNewRecord ? "Seleccione un poster" : $model->poster,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' => 'Seleccionar imagen'
        ],
    ]); ?>

    <?= $form->field($model, 'event_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Fecha del evento...'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'event_time')->widget(TimePicker::classname(), [
        'pluginOptions' => [
            'minuteStep' => 5
        ]
    ]);
    ?>

    <?= $form->field($model, 'event_time_average')->input('number', ['min' => 1, 'placeholder' => 'Duración promedio...']) ?>

    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'guesses')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->widget(\kartik\widgets\SwitchInput::classname(), [
        'name' => 'status',
        'pluginOptions' => ['size' => 'medium',
            'onText' => 'Activo',
            'offText' => 'Inactivo',
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
        <?= Html::a('Cancelar', Yii::$app->request->getReferrer(), ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
