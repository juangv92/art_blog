<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Artwork */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Artworks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artwork-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Crear', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Seguro desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'file_name',
                'value' => Html::img($model->generateFileRoute(),
                    ['alt' => $model->name, 'title' => $model->name, 'style'=>'width: 200px']),
                'format' => 'raw'
            ],
            'name',
            'author_id',
            'artwork_type_id',
            'exhibition_id',
            'technique_id',
            'm_unit_id',
            'abstract',
            [
                'attribute' => 'description', 
                'value' => $model->description,
                'format' => 'html'
            ],
            'release_date',
            [
                'attribute' => 'availability',
                'value' => function (\backend\models\Artwork $model) {
                    $url = \yii\helpers\Url::to(['/artwork/availability', 'id' => $model->id]);
                    return \kartik\switchinput\SwitchInput::widget([
                        'name' => 'status',
                        'value' => $model->status,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => 'En venta',
                            'offText' => 'No Disponible',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() {
                                        $.ajax('$url', {
                                            type: 'POST'
                                        }).done(function(data) {
                                            console.log(data);
                                        });
                                    }",
                        ]
                    ]);
                },
                'format' => 'raw'
            ],
            'quantity',
            'price',
            'raiting',
            'width',
            'height',
            'created_at',
            'updated_at',
            [
                'attribute' => 'status',
                'value' => function (\backend\models\Artwork $model) {
                    $url = \yii\helpers\Url::to(['/artwork/status', 'id' => $model->id]);
                    return \kartik\switchinput\SwitchInput::widget([
                        'name' => 'status',
                        'value' => $model->status,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => 'Activo',
                            'offText' => 'Inactivo',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() {
                                        $.ajax('$url', {
                                            type: 'POST'
                                        }).done(function(data) {
                                            console.log(data);
                                        });
                                    }",
                        ]
                    ]);
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
