<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ArtworkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Obras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artwork-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Obra', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(['id'=>'artworkGrid']); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'table-responsive'
        ],
        'columns' => [
            [
                'attribute' => 'id',
                'value' => 'id',
                'contentOptions' => ['style' => 'width:5%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'name',
                'value' => 'name',
                'contentOptions' => ['style' => 'width:10%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'artwork_type_id',
                'value' => 'artwork_type_id',
                'contentOptions' => ['style' => 'width:8%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'exhibition_id',
                'value' => 'exhibition_id',
                'contentOptions' => ['style' => 'width:10%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'technique_id',
                'value' => 'technique_id',
                'contentOptions' => ['style' => 'width:10%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'availability',
                'filter' => ["1" => "Disponible", "0" => "No Disponible"],
                'value' => function (\backend\models\Artwork $model) {
                    $url = \yii\helpers\Url::to(['/artwork/availability', 'id' => $model->id]);
                    return \kartik\widgets\SwitchInput::widget([
                        'name' => 'availability',
                        'value' => $model->availability,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() { 
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        console.log(data);
                                        $.pjax.reload({container: '#artworkGrid'});
                                    });
                                }",
                        ]
                    ]);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:8%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'quantity',
                'value' => 'quantity',
                'contentOptions' => ['style' => 'width:10%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'status',
                'filter' => ["1" => "Activo", "0" => "Inactivo"],
                'value' => function (\backend\models\Artwork $model) {
                    $url = \yii\helpers\Url::to(['/artwork/status', 'id' => $model->id]);
                    return \kartik\widgets\SwitchInput::widget([
                        'name' => 'status',
                        'value' => $model->status,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() { 
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        console.log(data);
                                        $.pjax.reload({container: '#artworkGrid'});
                                    });
                                }",
                        ]
                    ]);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:8%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'contentOptions' => ['style' => 'width:6%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
