<?php

use yii\helpers\Html;
use kartik\widgets\DatePicker;
use kartik\switchinput\SwitchInput;
use webvimark\modules\UserManagement\models\User;
use common\models\ArtBlogBase;
use kartik\widgets\FileInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Artwork */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="artwork-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'file')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            'showRemove' => false,
            'showPreview' => !$model->isNewRecord && $model->hasPoster(),
            'initialPreview' => [$model->generateFileRoute()],
            'initialPreviewConfig' => [
                ['caption' => $model->file_name],
            ],
            'initialPreviewAsData' => true,
            'overwriteInitial' => !$model->isNewRecord && $model->hasPoster(),
            'initialCaption' => $model->isNewRecord ? "Seleccione un poster" : $model->file_name,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' => 'Seleccionar imagen'
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'abstract')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(\yii\redactor\widgets\Redactor::className(), [
        'clientOptions' => [
            'lang' => 'es',
            'plugins' => ['clips', 'fontcolor', 'imagemanager'],
            'placeholder' => 'Descripción de la Obra'
        ]
    ]); ?>

    <?= $form->field($model, 'release_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Fecha de lanzamiento'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>


    <?= $form->field($model, 'artwork_type_id')->widget(Select2::classname(), [
        'language' => 'es',
        'data' => \backend\models\ArtworkType::getMappedArtworkTypes(),
        'options' => ['placeholder' => 'Seleccione el tipo de obra...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'exhibition_id')->widget(Select2::classname(), [
        'language' => 'es',
        'data' => \backend\models\Exhibition::getMappedExhibitions(),
        'options' => ['placeholder' => 'Seleccione el Evento...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'technique_id')->widget(Select2::classname(), [
        'language' => 'es',
        'data' => \backend\models\Technique::getMappedTechniques(),
        'options' => ['placeholder' => 'Seleccione la técnica...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'availability')->widget(SwitchInput::classname(), [
        'name' => 'availability',
        'pluginOptions' => ['size' => 'medium',
            'onText' => 'En venta',
            'offText' => 'No disponible',
        ],
    ]); ?>

    <?= $form->field($model, 'price', [
        'addon' => [
            'prepend' => ['content' => '$', 'options' => ['class' => 'alert-success']],
            'append' => ['content' => '.00', 'options' => ['style' => 'font-family: Monaco, Consolas, monospace;']],
        ]
    ]); ?>

    <?= $form->field($model, 'quantity')->input('number', ['min' => 1, 'placeholder' => 'Entre la cantidad de ejemplares']) ?>

    <?= $form->field($model, 'width')->input('number', ['min' => 1, 'placeholder' => 'Entre el ancho']) ?>

    <?= $form->field($model, 'height')->input('number', ['min' => 1, 'placeholder' => 'Entre el alto']) ?>

    <?= $form->field($model, 'm_unit_id')->widget(Select2::classname(), [
        'language' => 'es',
        'data' => \backend\models\MeasurementUnit::getMappedUM(),
        'options' => ['placeholder' => 'Seleccione la UM'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
        'name' => 'status',
        'pluginOptions' => ['size' => 'medium',
            'onText' => 'Activo',
            'offText' => 'Inactivo',
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
        <?= Html::a('Cancelar', Yii::$app->request->getReferrer(), ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
