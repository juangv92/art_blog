<?php

use yii\helpers\Html;
use kartik\widgets\DatePicker;
use kartik\switchinput\SwitchInput;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Review */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'curriculum_id')->widget(Select2::classname(), [
        'language' => 'es',
        'data' => \backend\models\Curriculum::getMappedCurriculums(),
        'options' => ['placeholder' => 'Selecciones el Artista'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'artwork_id')->widget(Select2::classname(), [
        'language' => 'es',
        'data' => \backend\models\Artwork::getMappedArtworks(),
        'options' => ['placeholder' => 'Seleccione la Obra...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'published_date')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Fecha'],
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'positive')->widget(SwitchInput::classname(), [
        'name' => 'positive',
        'pluginOptions' => ['size' => 'medium',
            'onText' => 'Constructiva',
            'offText' => 'Negativa',
        ],
    ]); ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
        'name' => 'status',
        'pluginOptions' => ['size' => 'medium',
            'onText' => 'Activo',
            'offText' => 'Inactivo',
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
        <?= Html::a('Cancelar', Yii::$app->request->getReferrer(), ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
