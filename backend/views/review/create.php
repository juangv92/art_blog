<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Review */

$this->title = 'Crear Crítica';
$this->params['breadcrumbs'][] = ['label' => 'Críticas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
