<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Review */

$this->title = $model->author . " (" . $model->created_at . ")";
$this->params['breadcrumbs'][] = ['label' => 'Críticas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Crear', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Seguro desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'author',
            [
                'attribute' => 'curriculum_id',
                'value' => $model->curriculum_id,
                'visible' => isset($model->curriculum_id)
            ],
            [
                'attribute' => 'artwork_id',
                'value' => $model->artwork_id,
                'visible' => isset($model->artwork_id)
            ],
            [
                'attribute' => 'positive',
                'value' => function (\backend\models\Review $model) {
                    $url = \yii\helpers\Url::to(['/review/positive', 'id' => $model->id]);
                    return \kartik\switchinput\SwitchInput::widget([
                        'name' => 'positive',
                        'value' => $model->positive,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => 'Constructiva',
                            'offText' => 'Negativa',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() {
                                        $.ajax('$url', {
                                            type: 'POST'
                                        }).done(function(data) {
                                            console.log(data);
                                        });
                                    }",
                        ]
                    ]);
                },
                'format' => 'raw'
            ],
            'description:ntext',
            'published_date',
            'created_at',
            'updated_at',
            [
                'attribute' => 'status',
                'value' => function (\backend\models\Review $model) {
                    $url = \yii\helpers\Url::to(['/review/status', 'id' => $model->id]);
                    return \kartik\switchinput\SwitchInput::widget([
                        'name' => 'status',
                        'value' => $model->status,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => 'Activo',
                            'offText' => 'Inactivo',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() {
                                        $.ajax('$url', {
                                            type: 'POST'
                                        }).done(function(data) {
                                            console.log(data);
                                        });
                                    }",
                        ]
                    ]);
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
