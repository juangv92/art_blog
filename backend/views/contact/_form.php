<?php

use yii\helpers\Html;
use kartik\widgets\DatePicker;
use kartik\switchinput\SwitchInput;
use webvimark\modules\UserManagement\models\User;
use common\models\ArtBlogBase;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Contact */
/* @var $form kartik\widgets\ActiveForm */
?>

<div class="contact-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icon')->textInput(['maxlength' => true, 'placeholder' => 'glyphicon glyphicon-phone']) ?>

    <?= $form->field($model, 'primary')->widget(SwitchInput::classname(), [
        'name' => 'primary',
        'pluginOptions' => ['size' => 'medium',
            'onText' => 'Si',
            'offText' => 'No',
            'onColor' => 'primary',
            'offColor' => 'info'
        ],
    ]); ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
        'name' => 'status',
        'pluginOptions' => ['size' => 'medium',
            'onText' => 'Activo',
            'offText' => 'Inactivo',
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
        <?= Html::a('Cancelar', Yii::$app->request->getReferrer(), ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
