<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Crear Contacto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php Pjax::begin(['id'=>'contactGrid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'value' => 'id',
                'contentOptions' => ['style' => 'width:5%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'icon',
                'value' => function (\backend\models\Contact $model){
                    return Html::tag('span','<i class="'. $model->icon .'"></i>');
                },
                'format' => 'html',
                'contentOptions' => ['style' => 'text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],

            ],
            [
                'attribute' => 'curriculum_id',
                'value' => 'curriculum_id',
                'contentOptions' => ['style' => 'width:5%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'value',
                'value' => 'value',
                'contentOptions' => ['style' => 'width:10%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'primary',
                'filter' => ["1" => "Primario", "0" => "Secundario"],
                'value' => function (\backend\models\Contact $model) {
                    $url = \yii\helpers\Url::to(['/contact/primary', 'id' => $model->id]);
                    return \kartik\widgets\SwitchInput::widget([
                        'name' => 'primary',
                        'value' => $model->primary,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() { 
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        console.log(data);
                                        $.pjax.reload({container: '#contactGrid'});
                                    });
                                }",
                        ]
                    ]);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:8%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'attribute' => 'status',
                'filter' => ["1" => "Activo", "0" => "Inactivo"],
                'value' => function (\backend\models\Contact $model) {
                    $url = \yii\helpers\Url::to(['/contact/status', 'id' => $model->id]);
                    return \kartik\widgets\SwitchInput::widget([
                        'name' => 'status',
                        'value' => $model->status,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => '<i class="glyphicon glyphicon-ok"></i>',
                            'offText' => '<i class="glyphicon glyphicon-remove"></i>',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() { 
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        console.log(data);
                                        $.pjax.reload({container: '#contactGrid'});
                                    });
                                }",
                        ]
                    ]);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'width:8%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'contentOptions' => ['style' => 'width:6%; text-align:center'],
                'headerOptions' => [
                    'style' => 'text-align:center',
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
