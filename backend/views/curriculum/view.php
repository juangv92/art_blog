<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Curriculum */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Curriculums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="curriculum-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Crear', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => 'Seguro desea eliminar este elemento?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'avatar',
                'value'=>Html::img($model->generateFileRoute(),
                    ['alt'=>$model->name,'title'=>$model->name, 'style'=>'width: 200px']),
                'format'=>'raw'
            ],
            'name',
            'last_name',
            'art_alias',
            'birthday',
            'birthplace',
            'current_place',
            [
                'attribute' => 'biography',
                'value' => $model->biography,
                'format' => 'html'
            ],
            'events:ntext',
            'created_at',
            'updated_at',
            [
                'attribute' => 'status',
                'value' => function (\backend\models\Curriculum $model) {
                    $url = \yii\helpers\Url::to(['/curriculum/status', 'id' => $model->id]);
                    return \kartik\switchinput\SwitchInput::widget([
                        'name' => 'status',
                        'value' => $model->status,
                        'pluginOptions' => [
                            'size' => 'mini',
                            'onText' => 'Activo',
                            'offText' => 'Inactivo',
                            'onColor' => 'success',
                            'offColor' => 'danger'
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() {
                                        $.ajax('$url', {
                                            type: 'POST'
                                        }).done(function(data) {
                                            console.log(data);
                                        });
                                    }",
                        ]
                    ]);
                },
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
