<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Curriculum */

$this->title = 'Crear Currículo';
$this->params['breadcrumbs'][] = ['label' => 'Currículos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="curriculum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
