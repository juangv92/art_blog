<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Curriculum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="curriculum-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'file')->widget(\kartik\widgets\FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions' => [
            'showUpload' => false,
            'showRemove' => false,
            'showPreview' => !$model->isNewRecord && $model->hasAvatar(),
            'initialPreview' => [$model->generateFileRoute()],
            'initialPreviewConfig' => [
                ['caption' => $model->avatar],
            ],
            'initialPreviewAsData' => true,
            'overwriteInitial' => !$model->isNewRecord && $model->hasAvatar(),
            'initialCaption' => $model->isNewRecord ? "Seleccione un perfil" : $model->avatar,
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' => 'Seleccionar imagen'
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'art_alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthday')->widget(\kartik\widgets\DatePicker::classname(), [
        'options' => ['placeholder' => 'Cumpleaños'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>
    <?= $form->field($model, 'birthplace')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'current_place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biography')->widget(\yii\redactor\widgets\Redactor::className(),[
        'clientOptions' => [
            'lang' => 'es',
            'plugins' => ['clips', 'fontcolor', 'imagemanager'],
            'placeholder'=>'Biografía personal'
        ]
    ]); ?>

    <?= $form->field($model, 'events')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->widget(\kartik\widgets\SwitchInput::classname(), [
        'name' => 'status',
        'pluginOptions' => ['size' => 'medium',
            'onText' => 'Activo',
            'offText' => 'Inactivo',
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
        <?= Html::a('Cancelar', Yii::$app->request->getReferrer(), ['class' => 'btn btn-default']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
