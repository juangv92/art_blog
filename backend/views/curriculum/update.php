<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Curriculum */

$this->title = 'Actualizar Currículo: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Currículos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="curriculum-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
