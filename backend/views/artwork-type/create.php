<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ArtworkType */

$this->title = 'Crear Tipo de Obra';
$this->params['breadcrumbs'][] = ['label' => 'Tipos de Obra', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artwork-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
