<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ArtworkType */

$this->title = 'Actualizar Tipo de Obra: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tipos de Obra', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="artwork-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
