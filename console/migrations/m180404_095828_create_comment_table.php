<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m180404_095828_create_comment_table extends Migration
{
    /**
     * Return the table name in two formats, for creating table (default), or for drop table
     * @param bool $forDrop tell which format return
     * @return string the table name
     */
    public static function tableName($forDrop = false)
    {
        return $forDrop ? 'comment' : '{{%comment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::tableName(), [
            'id' => $this->primaryKey(10)->notNull(),
            'parent_id' => $this->integer(10),
            'nick_name' => $this->string(255)->notNull(),
            'body' => $this->text()->notNull(),

            'status' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_comment_id', self::tableName(), 'parent_id', 'comment', 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (in_array(self::tableName(true), Yii::$app->db->schema->getTableNames())) {
            //Drop all relations with current table first and then you can drop the table
            $this->safeDropForeignKeys();
            $this->dropTable(self::tableName(true));
        }
    }

    /**
     * Drop safe all possible relations or foreign keys from other tables
     */
    private function safeDropForeignKeys()
    {
        //Dropping artwork relation
        if (in_array('comment', Yii::$app->db->schema->getTableNames())) {
            $this->dropForeignKey('fk_comment_id', 'comment');
        }
    }
}
