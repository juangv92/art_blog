<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact`.
 */
class m180404_095535_create_contact_table extends Migration
{
    /**
     * Return the table name in two formats, for creating table (default), or for drop table
     * @param bool $forDrop tell which format return
     * @return string the table name
     */
    public static function tableName($forDrop = false)
    {
        return $forDrop ? 'contact' : '{{%contact}}';
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::tableName(), [
            'id' => $this->primaryKey(10)->notNull(),
            'curriculum_id' => $this->integer(10)->notNull(),
            'value' => $this->string(255)->notNull(),
            'icon' => $this->string(255)->notNull(),
            'primary' => $this->boolean()->notNull()->defaultValue(false),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(true),
        ], $tableOptions);

        $this->addForeignKey('fk_curriculum_id', self::tableName(), 'curriculum_id', 'curriculum', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (in_array(self::tableName(true), Yii::$app->db->schema->getTableNames())) {
            $this->dropForeignKey("fk_curriculum_id",self::tableName(true));
            $this->dropTable(self::tableName(true));
        }
    }
}
