<?php

use yii\db\Migration;

/**
 * Handles the creation of table `curriculum`.
 */
class m180404_095431_create_curriculum_table extends Migration
{
    /**
     * Return the table name in two formats, for creating table (default), or for drop table
     * @param bool $forDrop tell which format return
     * @return string the table name
     */
    public static function tableName($forDrop = false)
    {
        return $forDrop ? 'curriculum' : '{{%curriculum}}';
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->safeDown();
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::tableName(), [
            'id' => $this->primaryKey(10)->notNull(),
            'avatar' => $this->string(255),
            'name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'art_alias' => $this->string(255)->notNull(),
            'birthday' => $this->date(),
            'birthplace' => $this->string(255),
            'current_place' => $this->string(255),
            'biography' => $this->text(),
            'events' => $this->text(),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(false),
        ], $tableOptions);

        //$this->addForeignKey('primary_key_user', self::tableName(), 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (in_array(self::tableName(true), Yii::$app->db->schema->getTableNames())) {
            //Drop all relations with current table first and then you can drop the table
            //$this->dropForeignKey("primary_key_user", self::tableName(true));
            $this->safeDropForeignKeys();
            $this->dropTable(self::tableName(true));
        }
    }

    /**
     * Drop safe all possible relations or foreign keys from other tables
     */
    private function safeDropForeignKeys()
    {
        //Dropping contact relation
        if (in_array('contact', Yii::$app->db->schema->getTableNames())) {
            $this->dropForeignKey('fk_curriculum_id', 'contact');
        }
        //Dropping artwork relation
        if (in_array('artwork', Yii::$app->db->schema->getTableNames())) {
            $this->dropForeignKey('fk_author_id', 'artwork');
        }
        //Dropping artwork relation
        if (in_array('review', Yii::$app->db->schema->getTableNames())) {
            $this->dropForeignKey('fk_review_curriculum_id', 'review');
        }
    }
}
