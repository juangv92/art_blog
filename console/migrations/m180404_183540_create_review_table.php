<?php

use yii\db\Migration;

/**
 * Handles the creation of table `review`.
 */
class m180404_183540_create_review_table extends Migration
{
    /**
     * Return the table name in two formats, for creating table (default), or for drop table
     * @param bool $forDrop tell which format return
     * @return string the table name
     */
    public static function tableName($forDrop = false)
    {
        return $forDrop ? 'review' : '{{%review}}';
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(self::tableName(), [
            'id' => $this->primaryKey(10)->notNull(),
            'author' => $this->string(255)->notNull(),
            'artwork_id' => $this->integer(10),
            'curriculum_id' => $this->integer(10),
            'description' => $this->text()->notNull(),
            'published_date' => $this->date(),
            'positive' => $this->boolean()->notNull()->defaultValue(true),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(false),
        ], $tableOptions);

        $this->addForeignKey('fk_review_curriculum_id', self::tableName(), 'curriculum_id', 'curriculum', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_review_artwork_id', self::tableName(), 'artwork_id', 'artwork', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (in_array(self::tableName(true), Yii::$app->db->schema->getTableNames())) {
            $this->dropForeignKey("fk_review_curriculum_id",self::tableName(true));
            $this->dropForeignKey("fk_review_artwork_id",self::tableName(true));
            $this->dropTable(self::tableName(true));
        }
    }
}
