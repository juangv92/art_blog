<?php

use yii\db\Migration;

/**
 * Handles the creation of table `artwork`.
 */
class m180404_182915_create_artwork_table extends Migration
{
    /**
     * Return the table name in two formats, for creating table (default), or for drop table
     * @param bool $forDrop tell which format return
     * @return string the table name
     */
    public static function tableName($forDrop = false)
    {
        return $forDrop ? 'artwork' : '{{%artwork}}';
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        echo "*** Creating table " . self::tableName() . " ***";

        $this->createTable(self::tableName(), [
            'id' => $this->primaryKey(10)->notNull(),
            'name' => $this->string(255)->notNull(),
            'author_id' => $this->integer(10)->notNull(),
            'artwork_type_id' => $this->integer(10)->notNull(),
            'exhibition_id' => $this->integer(10),
            'technique_id' => $this->integer(10)->notNull(),
            'm_unit_id' => $this->integer(10)->notNull(),
            'file_name' => $this->string(255),
            'abstract' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'release_date' => $this->date(),
            'availability' => $this->boolean()->notNull()->defaultValue(true),
            'quantity' => $this->integer()->notNull()->defaultValue(0),
            'price' => $this->float()->notNull()->defaultValue(0),
            'raiting' => $this->float()->notNull()->defaultValue(0),
            'width' => $this->float()->notNull()->defaultValue(0),
            'height' => $this->float()->notNull()->defaultValue(0),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(true),
        ], $tableOptions);

        $this->addForeignKey('fk_author_id', self::tableName(), 'author_id', 'curriculum', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_artwork_artwork_type_id', self::tableName(), 'artwork_type_id', 'artwork_type', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_artwork_exhibition_id', self::tableName(), 'exhibition_id', 'exhibition', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_artwork_technique_id', self::tableName(), 'technique_id', 'technique', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_artwork_m_unit_id', self::tableName(), 'm_unit_id', 'measurement_unit', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if (in_array(self::tableName(true), Yii::$app->db->schema->getTableNames())) {
            //Drop all relations with current table first and then you can drop the table
            $this->dropForeignKey("fk_author_id",self::tableName(true));
            $this->dropForeignKey("fk_artwork_artwork_type_id",self::tableName(true));
            $this->dropForeignKey("fk_artwork_exhibition_id",self::tableName(true));
            $this->dropForeignKey("fk_artwork_technique_id",self::tableName(true));
            $this->dropForeignKey("fk_artwork_m_unit_id",self::tableName(true));
            
            $this->safeDropForeignKeys();
            $this->dropTable(self::tableName(true));
        }
    }

    /**
     * Drop safe all possible relations or foreign keys from other tables
     */
    private function safeDropForeignKeys()
    {
        //Dropping artwork relation
        if (in_array('review', Yii::$app->db->schema->getTableNames())) {
            $this->dropForeignKey('fk_review_artwork_id', 'review');
        }
    }
}
