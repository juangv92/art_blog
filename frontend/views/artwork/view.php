<div class="page-container">
    <!-- BEGIN BREADCRUMBS -->
    <div class="row breadcrumbs margin-bottom-40">
        <div class="container">
            <div class="col-md-4 col-sm-4">
                <h1>Obra</h1>
            </div>
            <div class="col-md-8 col-sm-8">
                <ul class="pull-right breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Pages</a></li>
                    <li class="active">Portfolio Item</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- BEGIN CONTAINER -->
    <div class="container min-hight portfolio-page margin-bottom-30">
        <!-- BEGIN PORTFOLIO ITEM -->
        <div class="row">
            <!-- BEGIN CAROUSEL -->
            <div class="col-md-5 front-carousel">
                <div id="myCarousel" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <img src="<?= $artwork->generateFileRoute() ?> " alt="">
                            <div class="carousel-caption">
                                <p>Excepturi sint occaecati cupiditate non provident</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CAROUSEL -->

            <!-- BEGIN PORTFOLIO DESCRIPTION -->
            <div class="col-md-7">
                <h2><?= $artwork->name ?></h2>
                <br>
                <div class="row front-lists-v2 margin-bottom-15">
                    <div class="col-md-6">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-user"></i>AUTOR: <?= $artwork->author->name ?></li>
                            <li><i class="fa fa-tag"></i>CATEGORÍA: <?= $artwork->artworkType->name ?></li>
                            <li><i class="fa fa-home"></i>EXHIBICIÓN:  <?= $artwork->exhibition->name ?></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-dollar"></i>PRECIO: <?='$ ' . $artwork->price ?></li>
                            <li><i class="fa fa-road"></i>DIMESIONES: <?= $artwork->getDimension() ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <h2>Descripción</h2>
                <p><?= $artwork->description ?></p>
            </div>
            <!-- END PORTFOLIO DESCRIPTION -->
        </div>
        <!-- END PORTFOLIO ITEM -->


    </div>
    <!-- END CONTAINER -->
</div>