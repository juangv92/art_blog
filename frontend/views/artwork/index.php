<?php
use yii\helpers\Html;
use frontend\models\Artwork;

?>
    <div class="page-container">
        <!-- BEGIN BREADCRUMBS -->
        <div class="row breadcrumbs margin-bottom-40">
            <div class="container">
                <div class="col-md-4 col-sm-4">
                    <h1>Colección</h1>
                </div>
                <div class="col-md-8 col-sm-8">
                    <ul class="pull-right breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="">Pages</a></li>
                        <li class="active">Portfolio Item</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CONTAINER -->
                <div class="container min-hight portfolio-page margin-bottom-40">
                    <!-- BEGIN FILTER -->
                    <div class="filter-v1 margin-top-10">
                        <div class="row mix-grid thumbnails">
                            <?php foreach ($artworks as $artwork) : ?>
                                <div class="col-md-3 col-sm-4 mix category_1 mix_all ">
                                    <div class="mix-inner">
                                        <img class="img-responsive" src="<?= $artwork->generateFileRoute() ?>" alt="">
                                        <div class="mix-details">
                                            <h4><?= $artwork->name ?></h4>
                                            <?php echo Html::a('<i class="fa fa-link"></i>', ['/artwork/view', 'id' => $artwork->id], ['class' => 'mix-link']) ?>
                                            <a class="mix-preview fancybox-button" href="<?= $artwork->generateFileRoute() ?>"
                                           title="Project Name" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- END FILTER -->
                </div>
                <!-- END CONTAINER -->
            </div>
        </div>
    </div>
    <!-- BEGIN fast view of a product -->
    <div id="product-pop-up" style="display: none; width: 700px;">
        <div class="product-page product-pop-up">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-3">
                    <div class="product-main-image">
                        <img src="assets/temp/products/model7.jpg" alt="Cool green dress with red bell"
                             class="img-responsive">
                    </div>
                    <div class="product-other-images">
                        <a href="#" class="active"><img alt="Berry Lace Dress"
                                                        src="assets/temp/products/model3.jpg"></a>
                        <a href="#"><img alt="Berry Lace Dress" src="assets/temp/products/model4.jpg"></a>
                        <a href="#"><img alt="Berry Lace Dress" src="assets/temp/products/model5.jpg"></a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-9">
                    <h1>Cool green dress with red bell</h1>
                    <div class="price-availability-block clearfix">
                        <div class="price">
                            <strong><span>$</span>47.00</strong>
                            <em>$<span>62.00</span></em>
                        </div>
                        <div class="availability">
                            Availability: <strong>In Stock</strong>
                        </div>
                    </div>
                    <div class="description">
                        <p>Lorem ipsum dolor ut sit ame dolore adipiscing elit, sed nonumy nibh sed euismod laoreet
                            dolore magna aliquarm erat volutpat
                            Nostrud duis molestie at dolore.</p>
                    </div>
                    <div class="product-page-options">
                        <div class="pull-left">
                            <label class="control-label">Size:</label>
                            <select class="form-control input-sm">
                                <option>L</option>
                                <option>M</option>
                                <option>XL</option>
                            </select>
                        </div>
                        <div class="pull-left">
                            <label class="control-label">Color:</label>
                            <select class="form-control input-sm">
                                <option>Red</option>
                                <option>Blue</option>
                                <option>Black</option>
                            </select>
                        </div>
                    </div>
                    <div class="product-page-cart">
                        <div class="product-quantity">
                            <input id="product-quantity" type="text" value="1" readonly name="product-quantity"
                                   class="form-control input-sm">
                        </div>
                        <button class="btn btn-primary" type="submit">Add to cart</button>
                        <button class="btn btn-default" type="submit">More details</button>
                    </div>
                </div>

                <div class="sticker sticker-sale"></div>
            </div>
        </div>
    </div>
    <!-- END fast view of a product -->
<?php $this->registerJs(
    " Portfolio.init();"
) ?>