<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Artwork */

$this->title = 'Update Artwork: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Artworks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="artwork-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
