<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
    <div class="page-container">
        <!-- BEGIN BREADCRUMBS -->
        <div class="row breadcrumbs margin-bottom-40">
            <div class="container">
                <div class="col-md-4 col-sm-4">
                    <h1>Exposiciones</h1>
                </div>
                <div class="col-md-8 col-sm-8">
                    <ul class="pull-right breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li><a href="">Pages</a></li>
                        <li class="active">Portfolio Item</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN CONTAINER -->
                <div class="container min-hight portfolio-page margin-bottom-40">
                    <!-- BEGIN FILTER -->
                    <div class="filter-v1 margin-top-10">
                        <div class="row mix-grid thumbnails">
                            <?php foreach ($exhibitions as $exhibition) : ?>
                                <div class="col-md-3 col-sm-4 mix category_1 mix_all ">
                                    <div class="mix-inner">
                                        <img class="img-responsive" src="<?= $exhibition->generateFileRoute() ?>" alt="">
                                        <div class="mix-details">
                                            <h4><?= $exhibition->name ?></h4>
                                            <?php echo Html::a('<i class="fa fa-link"></i>', ['/exhibition/view' , 'id' => $exhibition->id ], ['class' => 'mix-link']) ?>
                                            <a class="mix-preview fancybox-button" href="<?= $exhibition->generateFileRoute() ?>"
                                               title="Project Name" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- END FILTER -->
                </div>
                <!-- END CONTAINER -->
            </div>
        </div>
    </div>
<?php $this->registerJs(
    " Portfolio.init();"
) ?>