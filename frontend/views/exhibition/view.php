<div class="page-container" xmlns="http://www.w3.org/1999/html">
    <!-- BEGIN BREADCRUMBS -->
    <div class="row breadcrumbs margin-bottom-40">
        <div class="container">
            <div class="col-md-4 col-sm-4">
                <h1>Exposición</h1>
            </div>
            <div class="col-md-8 col-sm-8">
                <ul class="pull-right breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Pages</a></li>
                    <li class="active">Portfolio Item</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END BREADCRUMBS -->
    <!-- BEGIN CONTAINER -->
    <div class="container min-hight portfolio-page margin-bottom-30">
        <!-- BEGIN PORTFOLIO ITEM -->
        <div class="row">
            <!-- BEGIN CAROUSEL -->
            <div class="col-md-5 front-carousel">
                <div id="myCarousel" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <img src="<?= $exhibition->generateFileRoute() ?>" alt="">
                            <div class="carousel-caption">
                                <p>Excepturi sint occaecati cupiditate non provident</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CAROUSEL -->

            <!-- BEGIN PORTFOLIO DESCRIPTION -->
            <div class="col-md-7">
                <h2><?= $exhibition->name ?></h2>
                <p><?=  $exhibition->description ?></p>
                <br>
                <div class="row front-lists-v2 margin-bottom-15">
                    <div class="col-md-6">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-calendar"></i>FECHA: <?= $exhibition->event_date ?></li>
                            <li><i class="fa fa-clock-o"></i>HORA: <?= $exhibition->event_time ?></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-clock-o"></i>TIEMPO DE DRURACIÓN: <?= $exhibition->event_time_average . ' Minutos' ?></li>
                            <li><i class="fa fa-location-arrow"></i>LUGAR: <?= $exhibition->place ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END PORTFOLIO DESCRIPTION -->
        </div>
        <div class="container min-hight gallery-page margin-bottom-40">
            <div class="row">
                <?php foreach ($artworks as $artwork) : ?>
                <div class="col-md-3 col-sm-2 gallery-item">
                    <a data-rel="fancybox-button" title="Project Name" href="assets/img/works/img1.jpg"
                       class="fancybox-button">
                        <img alt="" src="<?= $artwork->generateFileRoute() ?>" class="img-responsive">
                        <div class="zoomix"><i class="fa fa-search"></i></div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
            <!-- END CONTAINER -->
            <!-- END PORTFOLIO ITEM -->
        </div>
    </div>

