<?php

/* @var $this yii\web\View */

$this->title = 'Art Blog';
?>
    <div class="page-container">
        <!-- BEGIN REVOLUTION SLIDER -->
        <div class="fullwidthbanner-container slider-main">
            <div class="fullwidthabnner">
                <ul id="revolutionul" style="display:none;">
                    <!-- THE FIRST SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <?php echo ' <img src="' . \Yii::$app->request->baseUrl . '/img/sliders/revolution/bg1.jpg" alt="">'; ?>

                        <div class="caption lft slide_title slide_item_left"
                             data-x="0"
                             data-y="105"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutExpo">
                            Need a website design ?
                        </div>
                        <div class="caption lft slide_subtitle slide_item_left"
                             data-x="0"
                             data-y="180"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                            This is what you were looking for
                        </div>
                        <div class="caption lft slide_desc slide_item_left"
                             data-x="0"
                             data-y="220"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                            Lorem ipsum dolor sit amet, dolore eiusmod<br>
                            quis tempor incididunt. Sed unde omnis iste.
                        </div>
                        <a class="caption lft btn green slide_btn slide_item_left"
                           href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
                           data-x="0"
                           data-y="290"
                           data-speed="400"
                           data-start="3000"
                           data-easing="easeOutExpo">
                            Purchase Now!
                        </a>
                        <div class="caption lfb"
                             data-x="640"
                             data-y="55"
                             data-speed="700"
                             data-start="1000"
                             data-easing="easeOutExpo">
                            <?php echo '<img src="' . \Yii::$app->request->baseUrl . '/img/sliders/revolution/man-winner.png" alt="Image 1">'; ?>
                        </div>
                    </li>

                    <!-- THE SECOND SLIDE -->
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400"
                        data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                        <img src="assets/img/sliders/revolution/bg2.jpg" alt="">
                        <div class="caption lfl slide_title slide_item_left"
                             data-x="0"
                             data-y="125"
                             data-speed="400"
                             data-start="3500"
                             data-easing="easeOutExpo">
                            Powerfull & Clean
                        </div>
                        <div class="caption lfl slide_subtitle slide_item_left"
                             data-x="0"
                             data-y="200"
                             data-speed="400"
                             data-start="4000"
                             data-easing="easeOutExpo">
                            Responsive Admin & Website Theme
                        </div>
                        <div class="caption lfl slide_desc slide_item_left"
                             data-x="0"
                             data-y="245"
                             data-speed="400"
                             data-start="4500"
                             data-easing="easeOutExpo">
                            Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>
                        <div class="caption lfr slide_item_right"
                             data-x="635"
                             data-y="105"
                             data-speed="1200"
                             data-start="1500"
                             data-easing="easeOutBack">
                            <img src="assets/img/sliders/revolution/mac.png" alt="Image 1">
                        </div>
                        <div class="caption lfr slide_item_right"
                             data-x="580"
                             data-y="245"
                             data-speed="1200"
                             data-start="2000"
                             data-easing="easeOutBack">
                            <img src="assets/img/sliders/revolution/ipad.png" alt="Image 1">
                        </div>
                        <div class="caption lfr slide_item_right"
                             data-x="735"
                             data-y="290"
                             data-speed="1200"
                             data-start="2500"
                             data-easing="easeOutBack">
                            <img src="assets/img/sliders/revolution/iphone.png" alt="Image 1">
                        </div>
                        <div class="caption lfr slide_item_right"
                             data-x="835"
                             data-y="230"
                             data-speed="1200"
                             data-start="3000"
                             data-easing="easeOutBack">
                            <img src="assets/img/sliders/revolution/macbook.png" alt="Image 1">
                        </div>
                        <div class="caption lft slide_item_right"
                             data-x="865"
                             data-y="45"
                             data-speed="500"
                             data-start="5000"
                             data-easing="easeOutBack">
                            <img src="assets/img/sliders/revolution/hint1-blue.png" id="rev-hint1" alt="Image 1">
                        </div>
                        <div class="caption lfb slide_item_right"
                             data-x="355"
                             data-y="355"
                             data-speed="500"
                             data-start="5500"
                             data-easing="easeOutBack">
                            <img src="assets/img/sliders/revolution/hint2-blue.png" id="rev-hint2" alt="Image 1">
                        </div>

                    </li>

                    <!-- THE THIRD SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                        <img src="assets/img/sliders/revolution/bg3.jpg" alt="">
                        <div class="caption lfl slide_item_left"
                             data-x="20"
                             data-y="95"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutBack">
                            <iframe src="http://player.vimeo.com/video/56974716?portrait=0" width="420" height="240"
                                    style="border:0" allowFullScreen></iframe>
                        </div>
                        <div class="caption lfr slide_title"
                             data-x="470"
                             data-y="100"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                            Responsive Video Support
                        </div>
                        <div class="caption lfr slide_subtitle"
                             data-x="470"
                             data-y="170"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                            Youtube, Vimeo and others.
                        </div>
                        <div class="caption lfr slide_desc"
                             data-x="470"
                             data-y="220"
                             data-speed="400"
                             data-start="3000"
                             data-easing="easeOutExpo">
                            Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>
                        <a class="caption lfr btn yellow slide_btn" href=""
                           data-x="470"
                           data-y="280"
                           data-speed="400"
                           data-start="3500"
                           data-easing="easeOutExpo">
                            Watch more Videos!
                        </a>
                    </li>

                    <!-- THE FORTH SLIDE -->
                    <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                        data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                        <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                        <img src="assets/img/sliders/revolution/bg4.jpg" alt="">
                        <div class="caption lft slide_title"
                             data-x="0"
                             data-y="105"
                             data-speed="400"
                             data-start="1500"
                             data-easing="easeOutExpo">
                            What else included ?
                        </div>
                        <div class="caption lft slide_subtitle"
                             data-x="0"
                             data-y="180"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutExpo">
                            The Most Complete Admin Theme
                        </div>
                        <div class="caption lft slide_desc"
                             data-x="0"
                             data-y="225"
                             data-speed="400"
                             data-start="2500"
                             data-easing="easeOutExpo">
                            Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                        </div>
                        <a class="caption lft slide_btn btn red slide_item_left"
                           href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin" target="_blank"
                           data-x="0"
                           data-y="300"
                           data-speed="400"
                           data-start="3000"
                           data-easing="easeOutExpo">
                            Learn More!
                        </a>
                        <div class="caption lft start"
                             data-x="670"
                             data-y="55"
                             data-speed="400"
                             data-start="2000"
                             data-easing="easeOutBack">
                            <img src="assets/img/sliders/revolution/iphone_left.png" alt="Image 2">
                        </div>

                        <div class="caption lft start"
                             data-x="850"
                             data-y="55"
                             data-speed="400"
                             data-start="2400"
                             data-easing="easeOutBack">
                            <img src="assets/img/sliders/revolution/iphone_right.png" alt="Image 3">
                        </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
        </div>
        <!-- END REVOLUTION SLIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="container">
            <!-- BEGIN SERVICE BOX -->
            <div class="row service-box">
                <div class="col-md-4 col-sm-4">
                    <div class="service-box-heading">
                        <em><i class="fa fa-location-arrow blue"></i></em>
                        <span>Exposición</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde
                        nostrudlaboris. Sed unde omnis iste natus error sit voluptatem.</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="service-box-heading">
                        <em><i class="fa fa-location-arrow red"></i></em>
                        <span>Exposición</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde
                        nostrudlaboris. Sed unde omnis iste natus error sit voluptatem.</p>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="service-box-heading">
                        <em><i class="fa fa-location-arrow green"></i></em>
                        <span>Exposición</span>
                    </div>
                    <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde
                        nostrudlaboris. Sed unde omnis iste natus error sit voluptatem.</p>
                </div>
            </div>
            <!-- END SERVICE BOX -->

            <!-- BEGIN BLOCKQUOTE BLOCK -->
            <div class="row quote-v1">
                <div class="col-md-9 quote-v1-inner">
                    <span>Lorem Insump</span>
                </div>
                <!--                <div class="col-md-3 quote-v1-inner text-right">-->
                <!--                    <a class="btn-transparent" href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin" target="_blank"><i class="fa fa-rocket margin-right-10"></i>Preview Admin</a>-->
                <!--                    <!--<a class="btn-transparent" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469"><i class="icon-gift margin-right-10"></i>Purchase 2 in 1</a>-->
                <!--                </div>-->
            </div>
            <!-- END BLOCKQUOTE BLOCK -->

            <div class="clearfix"></div>

            <!-- BEGIN RECENT WORKS -->
            <div class="row recent-work margin-bottom-40">
                <div class="col-md-3">
                    <h2><a href="portfolio.html">Obras Recientes</a></h2>
                    <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde
                        voluptatem. Sed unde omnis iste natus error sit voluptatem.</p>
                </div>
                <div class="col-md-9">
                    <ul class="bxslider">
                        <li>
                            <em>
                                <?php echo ' <img src="' . Yii::$app->request->baseUrl . '/img/artwork/guernica.jpg' . '" alt="" /> '; ?>
                                <a href="portfolio_item.html"><i class="fa fa-link icon-hover icon-hover-1"></i></a>
                                <a href="assets/img/works/img1.jpg" class="fancybox-button" title="Project Name #1"
                                   data-rel="fancybox-button"><i class="fa fa-search icon-hover icon-hover-2"></i></a>
                            </em>
                            <a class="bxslider-block" href="#">
                                <strong>Guernica</strong>
                                <b>Picasso</b>
                            </a>
                        </li>
                        <li>
                            <em>
                                <?php echo ' <img src="' . Yii::$app->request->baseUrl . '/img/artwork/mona.jpg' . '" alt="" /> '; ?>
                                <a href="portfolio_item.html"><i class="fa fa-link icon-hover icon-hover-1"></i></a>
                                <a href="assets/img/works/img1.jpg" class="fancybox-button" title="Project Name #1"
                                   data-rel="fancybox-button"><i class="fa fa-search icon-hover icon-hover-2"></i></a>
                            </em>
                            <a class="bxslider-block" href="#">
                                <strong>Geoconda</strong>
                                <b>Leonardo DaVinci</b>
                            </a>
                        </li>
                        <li>
                            <em>
                                <?php echo ' <img src="' . Yii::$app->request->baseUrl . '/img/artwork/guernica.jpg' . '" alt="" /> '; ?>
                                <a href="portfolio_item.html"><i class="fa fa-link icon-hover icon-hover-1"></i></a>
                                <a href="assets/img/works/img1.jpg" class="fancybox-button" title="Project Name #1"
                                   data-rel="fancybox-button"><i class="fa fa-search icon-hover icon-hover-2"></i></a>
                            </em>
                            <a class="bxslider-block" href="#">
                                <strong>Guernica</strong>
                                <b>Picasso</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END RECENT WORKS -->

            <div class="clearfix"></div>

            <!-- BEGIN TABS AND TESTIMONIALS -->
            <div class="row mix-block">

                <!-- TESTIMONIALS -->
                <div class="testimonials-v1">
                    <div id="myCarousel" class="carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="active item">
                                <span class="testimonials-slide">Denim you probably haven't heard of. Lorem ipsum dolor met consectetur adipisicing sit amet, consectetur adipisicing elit, of them jean shorts sed magna aliqua. Lorem ipsum dolor met consectetur adipisicing sit amet do eiusmod dolore.</span>
                                <div class="carousel-info">
                                    <img class="pull-left"
                                         src="<?= Yii::$app->getHomeUrl() . "img/people/img1-small.jpg"; ?>" alt=""/>
                                    <div class="pull-left">
                                        <span class="testimonials-name">Criticas Recientes</span>
                                        <span class="testimonials-post">Critico</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <span class="testimonials-slide">Raw denim you Mustache cliche tempor, williamsburg carles vegan helvetica probably haven't heard of them jean shorts austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</span>
                                <div class="carousel-info">
                                    <img class="pull-left"
                                         src="<?= Yii::$app->getHomeUrl() . "img/people/img1-small.jpg"; ?>" alt=""/>
                                    <div class="pull-left">
                                        <span class="testimonials-name">Criticas Recientes</span>
                                        <span class="testimonials-post">Critico</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Carousel nav -->
                        <a class="left-btn" href="#myCarousel" data-slide="prev"></a>
                        <a class="right-btn" href="#myCarousel" data-slide="next"></a>
                    </div>
                </div>
                <!-- END TESTIMONIALS -->
            </div>
            <!-- END TABS AND TESTIMONIALS -->


        </div>
        <!-- END CONTAINER -->
    </div>
<?php $this->registerJs(
    " 
    App.initBxSlider();
    Index.initRevolutionSlider();"
) ?>