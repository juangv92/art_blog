<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?= Yii::$app->getHomeUrl() . "favicon.ico"; ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <!-- BEGIN TOP BAR -->
    <div class="front-topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-unstyle inline">
                        <li><i class="icon-phone topbar-info-icon top-2"></i>Call us: <span>(1) 456 6717</span></li>
                        <li class="sep"><span>|</span></li>
                        <li><i class="icon-envelope-alt topbar-info-icon top-2"></i>Email:
                            <span>info@keenthemes.com</span></li>
                    </ul>
                </div>
                <div class="col-md-6 topbar-social">
                    <ul class="list-unstyled inline">
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-google-plus"></i></a></li>
                        <li><a href="#"><i class="icon-linkedin"></i></a></li>
                        <li><a href="#"><i class="icon-youtube"></i></a></li>
                        <li><a href="#"><i class="icon-skype"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END TOP BAR -->

    <div class="header navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <button class="navbar-toggle btn navbar-btn" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN LOGO (you can use logo image instead of text)-->
                <a class="navbar-brand logo-v1" href="index.html">
                   <?php echo '<img src="' . \Yii::$app->request->baseUrl . '/img/logo_gallery_blue.png' . '" id="logoimg" alt="">'; ?>
                </a>
                <!-- END LOGO -->
            </div>
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><?php echo Html::a('Inicio', ['/site/index']) ?></li>
                    <li><?php echo Html::a('Colección',['/artwork/index']) ?></li>
                    <li><?php echo Html::a('Exposiciones',['/exhibition/index']) ?></li>
                    <li><?php echo Html::a('Galería',['/artwork/index']) ?></li>
                    <li><?php echo Html::a('Contacto',['/artwork/index']) ?></li>
                    <li class="menu-search">
                        <span class="sep"></span>
                        <i class="icon-search search-btn"></i>

                        <div class="search-box">
                            <div class="input-group">
                                <form>
                                    <input style="background:#fff;" class="input-sm m-wrap form-control" type="text"
                                           placeholder="Search"/>
                                    <button type="submit" class="btn theme-btn">Go</button>
                                </form>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- BEGIN TOP NAVIGATION MENU -->
        </div>
    </div>

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode("Art Blog") ?> <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
