<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',

        'plugins/font-awesome/css/font-awesome.min.css',
        'plugins/bootstrap/css/bootstrap.min.css',

        'plugins/fancybox/source/jquery.fancybox.css',
        'plugins/revolution_slider/css/rs-style.css',
        'plugins/revolution_slider/rs-plugin/css/settings.css',
        'plugins/bxslider/jquery.bxslider.css',
        'css/pages/prices.css',

        'css/style-metronic.css',
        'css/pages/portfolio.css',
        'css/custom.css',
        'css/style.css',
        'css/style-responsive.css',
        'css/themes/blue.css',
        'css/pages/gallery.css',
    ];
    public $js = [
        'plugins/respond.min.js',

        'plugins/jquery-1.10.2.min.js',
        'plugins/jquery-migrate-1.2.1.min.js',
        'plugins/bootstrap/js/bootstrap.min.js',
        'plugins/back-to-top.js',

        'plugins/fancybox/source/jquery.fancybox.pack.js',
        'plugins/hover-dropdown.js',
        'plugins/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js',
        'plugins/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js',
        'plugins/bxslider/jquery.bxslider.min.js',

        'plugins/jquery-1.10.1.min.js',
        'plugins/jquery.mixitup.min.js',
        'scripts/app.js',
        'scripts/index.js',
        'scripts/portfolio.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
