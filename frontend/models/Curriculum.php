<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%curriculum}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $last_name
 * @property string $art_alias
 * @property string $birthday
 * @property string $birthplace
 * @property string $current_place
 * @property string $biography
 *
 * @property Artwork[] $artworks
 * @property Contact[] $contacts
 * @property User $user
 * @property Review[] $reviews
 */
class Curriculum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%curriculum}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'last_name', 'art_alias'], 'required'],
            [['user_id'], 'integer'],
            [['birthday'], 'safe'],
            [['biography'], 'string'],
            [['name', 'last_name', 'art_alias', 'birthplace', 'current_place'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'last_name' => 'Last Name',
            'art_alias' => 'Art Alias',
            'birthday' => 'Birthday',
            'birthplace' => 'Birthplace',
            'current_place' => 'Current Place',
            'biography' => 'Biography',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtworks()
    {
        return $this->hasMany(Artwork::className(), ['author_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['curriculum_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['curriculum_id' => 'id']);
    }
}
