<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%event}}".
 *
 * @property int $id
 * @property string $name
 * @property string $place
 * @property string $event_date
 * @property string $event_time
 * @property string $event_time_average
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Exhibition[] $exhibitions
 * @property Review[] $reviews
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'place', 'created_at', 'updated_at'], 'required'],
            [['event_date', 'created_at', 'updated_at'], 'safe'],
            [['name', 'place'], 'string', 'max' => 255],
            [['event_time', 'event_time_average'], 'string', 'max' => 10],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'place' => 'Place',
            'event_date' => 'Event Date',
            'event_time' => 'Event Time',
            'event_time_average' => 'Event Time Average',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExhibitions()
    {
        return $this->hasMany(Exhibition::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['event_id' => 'id']);
    }
}
