<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%artwork}}".
 *
 * @property int $id
 * @property string $name
 * @property int $author_id
 * @property int $artwork_type_id
 * @property int $exhibition_id
 * @property int $technique_id
 * @property int $m_unit_id
 * @property string $file_name
 * @property string $abstract
 * @property string $description
 * @property string $release_date
 * @property int $availability
 * @property int $quantity
 * @property double $price
 * @property double $raiting
 * @property double $width
 * @property double $height
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 *
 * @property ArtworkType $artworkType
 * @property Exhibition $exhibition
 * @property MeasurementUnit $mUnit
 * @property Technique $technique
 * @property Curriculum $author
 * @property Review[] $reviews
 */
class Artwork extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%artwork}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'author_id', 'artwork_type_id', 'technique_id', 'm_unit_id', 'abstract', 'created_at', 'updated_at'], 'required'],
            [['author_id', 'artwork_type_id', 'exhibition_id', 'technique_id', 'm_unit_id', 'quantity'], 'integer'],
            [['description'], 'string'],
            [['release_date', 'created_at', 'updated_at'], 'safe'],
            [['price', 'raiting', 'width', 'height'], 'number'],
            [['name', 'file_name', 'abstract'], 'string', 'max' => 255],
            [['availability', 'status'], 'string', 'max' => 1],
            [['artwork_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArtworkType::className(), 'targetAttribute' => ['artwork_type_id' => 'id']],
            [['exhibition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Exhibition::className(), 'targetAttribute' => ['exhibition_id' => 'id']],
            [['m_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => MeasurementUnit::className(), 'targetAttribute' => ['m_unit_id' => 'id']],
            [['technique_id'], 'exist', 'skipOnError' => true, 'targetClass' => Technique::className(), 'targetAttribute' => ['technique_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Curriculum::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'author_id' => 'Author ID',
            'artwork_type_id' => 'Artwork Type ID',
            'exhibition_id' => 'Exhibition ID',
            'technique_id' => 'Technique ID',
            'm_unit_id' => 'M Unit ID',
            'file_name' => 'File Name',
            'abstract' => 'Abstract',
            'description' => 'Description',
            'release_date' => 'Release Date',
            'availability' => 'Availability',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'raiting' => 'Raiting',
            'width' => 'Width',
            'height' => 'Height',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtworkType()
    {
        return $this->hasOne(ArtworkType::className(), ['id' => 'artwork_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExhibition()
    {
        return $this->hasOne(Exhibition::className(), ['id' => 'exhibition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMUnit()
    {
        return $this->hasOne(MeasurementUnit::className(), ['id' => 'm_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnique()
    {
        return $this->hasOne(Technique::className(), ['id' => 'technique_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Curriculum::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['artwork_id' => 'id']);
    }

    /** Returns the relative path to avatar file
     * @return string
     */
    public function generateFileRoute()
    {
        return "../../" . Yii::getAlias("@resources") . '/artwork_files/Artwork_' . $this->id . "/" . $this->file_name;
    }
}
