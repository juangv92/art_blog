<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%exhibition}}".
 *
 * @property int $id
 * @property string $name
 * @property string $poster
 * @property string $abstract
 * @property string $description
 * @property string $event_date
 * @property string $event_time
 * @property string $event_time_average
 * @property string $place
 * @property string $guesses
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 *
 * @property Artwork[] $artworks
 */
class Exhibition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exhibition}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'abstract', 'place', 'created_at', 'updated_at'], 'required'],
            [['description', 'guesses'], 'string'],
            [['event_date', 'created_at', 'updated_at'], 'safe'],
            [['name', 'poster', 'abstract', 'place'], 'string', 'max' => 255],
            [['event_time', 'event_time_average'], 'string', 'max' => 10],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'poster' => 'Poster',
            'abstract' => 'Abstract',
            'description' => 'Description',
            'event_date' => 'Event Date',
            'event_time' => 'Event Time',
            'event_time_average' => 'Event Time Average',
            'place' => 'Place',
            'guesses' => 'Guesses',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtworks()
    {
        return $this->hasMany(Artwork::className(), ['exhibition_id' => 'id']);
    }

    public function generateFileRoute()
    {
        return "../../" . Yii::getAlias("@resources") . '/exhibition_files/Event_' . $this->id . "/" . $this->poster;
    }
}
