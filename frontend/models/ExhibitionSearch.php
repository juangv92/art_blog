<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Exhibition;

/**
 * ExhibitionSearch represents the model behind the search form of `frontend\models\Exhibition`.
 */
class ExhibitionSearch extends Exhibition
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'poster', 'abstract', 'description', 'event_date', 'event_time', 'event_time_average', 'place', 'guesses', 'created_at', 'updated_at', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Exhibition::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'event_date' => $this->event_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'poster', $this->poster])
            ->andFilterWhere(['like', 'abstract', $this->abstract])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'event_time', $this->event_time])
            ->andFilterWhere(['like', 'event_time_average', $this->event_time_average])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'guesses', $this->guesses])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
