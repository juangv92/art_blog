-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2018 a las 20:47:04
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `art_blog`
--
CREATE DATABASE IF NOT EXISTS `art_blog` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `art_blog`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `artwork`
--

CREATE TABLE IF NOT EXISTS `artwork` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `author_id` int(10) NOT NULL,
  `artwork_type_id` int(10) NOT NULL,
  `exhibition_id` int(10) NOT NULL,
  `technique_id` int(10) NOT NULL,
  `m_unit_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `release_date` date DEFAULT NULL,
  `availability` tinyint(1) NOT NULL DEFAULT '1',
  `quantity` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `width` float NOT NULL DEFAULT '0',
  `height` float NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_author_id` (`author_id`),
  KEY `fk_artwork_artwork_type_id` (`artwork_type_id`),
  KEY `fk_artwork_exhibition_id` (`exhibition_id`),
  KEY `fk_artwork_technique_id` (`technique_id`),
  KEY `fk_artwork_m_unit_id` (`m_unit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `artwork_type`
--

CREATE TABLE IF NOT EXISTS `artwork_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `group_code` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  KEY `fk_auth_item_group_code` (`group_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`, `group_code`) VALUES
('/*', 3, NULL, NULL, NULL, 1522856333, 1522856333, NULL),
('//*', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('//controller', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('//crud', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('//extension', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('//form', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('//index', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('//model', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('//module', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/asset/*', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/asset/compress', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/asset/template', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/cache/*', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/cache/flush', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/cache/flush-all', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/cache/flush-schema', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/cache/index', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/fixture/*', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/fixture/load', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/fixture/unload', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/gii/*', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/gii/default/*', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/gii/default/action', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/gii/default/diff', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/gii/default/index', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/gii/default/preview', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/gii/default/view', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/help/*', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/help/index', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/help/list', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/help/list-action-options', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/help/usage', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/message/*', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/message/config', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/message/config-template', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/message/extract', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/*', 3, NULL, NULL, NULL, 1522856333, 1522856333, NULL),
('/migrate/create', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/down', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/fresh', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/history', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/mark', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/new', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/redo', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/to', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/migrate/up', 3, NULL, NULL, NULL, 1522856334, 1522856334, NULL),
('/serve/*', 3, NULL, NULL, NULL, 1522856333, 1522856333, NULL),
('/serve/index', 3, NULL, NULL, NULL, 1522856333, 1522856333, NULL),
('/user-management/*', 3, NULL, NULL, NULL, 1522856335, 1522856335, NULL),
('/user-management/auth/change-own-password', 3, NULL, NULL, NULL, 1522856338, 1522856338, NULL),
('/user-management/user-permission/set', 3, NULL, NULL, NULL, 1522856337, 1522856337, NULL),
('/user-management/user-permission/set-roles', 3, NULL, NULL, NULL, 1522856337, 1522856337, NULL),
('/user-management/user/bulk-activate', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('/user-management/user/bulk-deactivate', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('/user-management/user/bulk-delete', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('/user-management/user/change-password', 3, NULL, NULL, NULL, 1522856337, 1522856337, NULL),
('/user-management/user/create', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('/user-management/user/delete', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('/user-management/user/grid-page-size', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('/user-management/user/index', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('/user-management/user/update', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('/user-management/user/view', 3, NULL, NULL, NULL, 1522856336, 1522856336, NULL),
('Admin', 1, 'Admin', NULL, NULL, 1522856335, 1522856335, NULL),
('assignRolesToUsers', 2, 'Assign roles to users', NULL, NULL, 1522856337, 1522856337, 'userManagement'),
('bindUserToIp', 2, 'Bind user to IP', NULL, NULL, 1522856337, 1522856337, 'userManagement'),
('changeOwnPassword', 2, 'Change own password', NULL, NULL, 1522856338, 1522856338, 'userCommonPermissions'),
('changeUserPassword', 2, 'Change user password', NULL, NULL, 1522856337, 1522856337, 'userManagement'),
('commonPermission', 2, 'Common permission', NULL, NULL, 1522856333, 1522856333, NULL),
('createUsers', 2, 'Create users', NULL, NULL, 1522856336, 1522856336, 'userManagement'),
('deleteUsers', 2, 'Delete users', NULL, NULL, 1522856336, 1522856336, 'userManagement'),
('editUserEmail', 2, 'Edit user email', NULL, NULL, 1522856337, 1522856337, 'userManagement'),
('editUsers', 2, 'Edit users', NULL, NULL, 1522856336, 1522856336, 'userManagement'),
('viewRegistrationIp', 2, 'View registration IP', NULL, NULL, 1522856337, 1522856337, 'userManagement'),
('viewUserEmail', 2, 'View user email', NULL, NULL, 1522856337, 1522856337, 'userManagement'),
('viewUserRoles', 2, 'View user roles', NULL, NULL, 1522856337, 1522856337, 'userManagement'),
('viewUsers', 2, 'View users', NULL, NULL, 1522856336, 1522856336, 'userManagement'),
('viewVisitLog', 2, 'View visit log', NULL, NULL, 1522856337, 1522856337, 'userManagement');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('changeOwnPassword', '/user-management/auth/change-own-password'),
('assignRolesToUsers', '/user-management/user-permission/set'),
('assignRolesToUsers', '/user-management/user-permission/set-roles'),
('editUsers', '/user-management/user/bulk-activate'),
('editUsers', '/user-management/user/bulk-deactivate'),
('deleteUsers', '/user-management/user/bulk-delete'),
('changeUserPassword', '/user-management/user/change-password'),
('createUsers', '/user-management/user/create'),
('deleteUsers', '/user-management/user/delete'),
('viewUsers', '/user-management/user/grid-page-size'),
('viewUsers', '/user-management/user/index'),
('editUsers', '/user-management/user/update'),
('viewUsers', '/user-management/user/view'),
('Admin', 'assignRolesToUsers'),
('Admin', 'changeOwnPassword'),
('Admin', 'changeUserPassword'),
('Admin', 'createUsers'),
('Admin', 'deleteUsers'),
('Admin', 'editUsers'),
('editUserEmail', 'viewUserEmail'),
('assignRolesToUsers', 'viewUserRoles'),
('Admin', 'viewUsers'),
('assignRolesToUsers', 'viewUsers'),
('changeUserPassword', 'viewUsers'),
('createUsers', 'viewUsers'),
('deleteUsers', 'viewUsers'),
('editUsers', 'viewUsers');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item_group`
--

CREATE TABLE IF NOT EXISTS `auth_item_group` (
  `code` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_item_group`
--

INSERT INTO `auth_item_group` (`code`, `name`, `created_at`, `updated_at`) VALUES
('userCommonPermissions', 'User common permission', 1522856337, 1522856337),
('userManagement', 'User management', 1522856336, 1522856336);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `nick_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comment_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `curriculum_id` int(10) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_curriculum_id` (`curriculum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curriculum`
--

CREATE TABLE IF NOT EXISTS `curriculum` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `art_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `birthplace` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` text COLLATE utf8_unicode_ci,
  KEY `primary_key_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `event_date` date DEFAULT NULL,
  `event_time` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '--',
  `event_time_average` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '--',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exhibition`
--

CREATE TABLE IF NOT EXISTS `exhibition` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `event_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `abstract` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_exhibition_event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `measurement_unit`
--

CREATE TABLE IF NOT EXISTS `measurement_unit` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1522866760),
('m130524_201442_init', 1522867555),
('m180404_095431_create_curriculum_table', 1522867555),
('m180404_095535_create_contact_table', 1522867555),
('m180404_095554_create_artwork_type_table', 1522867555),
('m180404_095613_create_measurement_unit_table', 1522867555),
('m180404_095628_create_technique_table', 1522867555),
('m180404_095655_create_event_table', 1522867555),
('m180404_095732_create_exhibition_table', 1522867555),
('m180404_095828_create_comment_table', 1522867555),
('m180404_182915_create_artwork_table', 1522867556),
('m180404_183540_create_review_table', 1522867556);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `curriculum_id` int(10) DEFAULT NULL,
  `event_id` int(10) DEFAULT NULL,
  `artwork_id` int(10) DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `published_date` date DEFAULT NULL,
  `positive` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_review_curriculum_id` (`curriculum_id`),
  KEY `fk_review_event_id` (`event_id`),
  KEY `fk_review_artwork_id` (`artwork_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `technique`
--

CREATE TABLE IF NOT EXISTS `technique` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `superadmin` smallint(6) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `registration_ip` varchar(15) DEFAULT NULL,
  `bind_to_ip` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `email_confirmed` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `confirmation_token`, `status`, `superadmin`, `created_at`, `updated_at`, `registration_ip`, `bind_to_ip`, `email`, `email_confirmed`) VALUES
(1, 'superadmin', 'S3KPRM9agNt98MnwKfTfwGlRtgb6j-VG', '$2y$13$Yzur0rrXyV8hRrKM7R30leNDSON4.X9cyKY/QDCx4vDnziC1kLpfu', NULL, 1, 1, 1522856332, 1522856332, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_visit_log`
--

CREATE TABLE IF NOT EXISTS `user_visit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `language` char(2) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `visit_time` int(11) NOT NULL,
  `browser` varchar(30) DEFAULT NULL,
  `os` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `artwork`
--
ALTER TABLE `artwork`
  ADD CONSTRAINT `fk_artwork_m_unit_id` FOREIGN KEY (`m_unit_id`) REFERENCES `measurement_unit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_artwork_artwork_type_id` FOREIGN KEY (`artwork_type_id`) REFERENCES `artwork_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_artwork_exhibition_id` FOREIGN KEY (`exhibition_id`) REFERENCES `exhibition` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_artwork_technique_id` FOREIGN KEY (`technique_id`) REFERENCES `technique` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_author_id` FOREIGN KEY (`author_id`) REFERENCES `curriculum` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `fk_auth_item_group_code` FOREIGN KEY (`group_code`) REFERENCES `auth_item_group` (`code`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_comment_id` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `fk_curriculum_id` FOREIGN KEY (`curriculum_id`) REFERENCES `curriculum` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `curriculum`
--
ALTER TABLE `curriculum`
  ADD CONSTRAINT `primary_key_user` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `exhibition`
--
ALTER TABLE `exhibition`
  ADD CONSTRAINT `fk_exhibition_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `fk_review_artwork_id` FOREIGN KEY (`artwork_id`) REFERENCES `artwork` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_review_curriculum_id` FOREIGN KEY (`curriculum_id`) REFERENCES `curriculum` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_review_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user_visit_log`
--
ALTER TABLE `user_visit_log`
  ADD CONSTRAINT `user_visit_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
